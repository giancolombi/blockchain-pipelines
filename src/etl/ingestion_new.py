#!/usr/bin/python3
import click
import os.path
import pandas as pd
import numpy as np
import sqlite3
from sqlite3 import Error
import smtplib
import datetime
from datetime import date
import yaml
from pathlib import Path
import logging

def connect_to_db(db_path):
    """
    Connect to an SQlite database, if db file does not exist it will be created
    :param db_file: absolute or relative path of db file
    :return: sqlite3 connection
    """
    sqlite3_conn = None

    try:
        sqlite3_conn = sqlite3.connect(db_path)
        return sqlite3_conn

    except Error as err:
        print(err)

        if sqlite3_conn is not None:
            sqlite3_conn.close()

def get_credentials(config_file):
    
    my_path = os.path.abspath(os.path.dirname(__file__))
    path = str(os.path.join(my_path, config_file))

    with open(path, 'r') as stream:
        try:
            cfg = yaml.safe_load(stream)
            sender_email = cfg['email_service']['sender_email']
            sender_password = cfg['email_service']['sender_password']
            recipeint_emails = cfg['email_service']['recipeint_emails']
            conn = {'sender_emails': sender_email, 'sender_password': sender_password, 'recipeint_email': recipeint_emails}
        except yaml.YAMLError as exc:
            print(exc)
    return conn

def update_table_stats(db_path, table):
    # # Create a SQL connection to our SQLite database
    con = sqlite3.connect(db_path)

    if con is not None:
        cur = con.cursor()
        # Create table if it is not exist
        cur.execute(
            'CREATE TABLE IF NOT EXISTS table_stats' +
            '(table_name               VARCHAR,'
            'latest_row_count          INTEGER,'
            'lastest_etl_timestamp     INTEGER)'
        )

        for row in cur.execute('SELECT count(1) FROM ' + table + ';'):
            updated_row_count = row[0]

        d = {'table_name': [table], 'latest_row_count': [updated_row_count], 'lastest_etl_timestamp': [datetime.datetime.now()]}
        df = pd.DataFrame(data=d)
        
        df.to_sql(name="table_stats", con=con, if_exists='append', index=False)
        con.close()

    else:
        print('Connection to database failed')

def update_db(db_path, table_name, df):
    # # Create a SQL connection to our SQLite database
    con = sqlite3.connect(db_path)

    if con is not None:
        cur = con.cursor()
        # Create table if it is not exist
        cur.execute(
            'CREATE TABLE IF NOT EXISTS ' + table_name +
            '(city        VARCHAR,'
            'state        VARCHAR,'
            'country      VARCHAR,'
            'postcode     VARCHAR)'
        )
        
        df.to_sql(name=table_name, con=con, if_exists='append', index=False)
        update_table_stats(db_path, table_name)
        con.close()

    else:
        print('Connection to database failed')

def trim_all_columns(df):
    """
    Trim whitespace from ends of each value across all series in dataframe
    """
    trim_strings = lambda x: x.strip() if isinstance(x, str) else x
    return df.applymap(trim_strings)

## will send an email reporting given statistics about the db
def run_report(table_name, config_file):

    cfg = get_credentials(config_file)

    # print(f'Sender email is: {db_path}')
    # print(f'Receipent email are: {table}')

    ## no hardcoded information here
    ## you would want to use a config file for this information
    sender_email = cfg['sender_email']
    sender_password = cfg['sender_password']
    

    ## also with more time you would not want to expose your users
    ## this list will be in a config file
    to = cfg['recipeint_emails']
    subject = table_name + ' Has been updated!'
    body = 'Record count: ' + \
            ' With your top performing zipcode: ' + \
            ' with: ' + ' records in the db'
    
    email_text = """\
    From: %s
    To: %s
    Subject: %s

    %s
    """ % (sender_email, ", ".join(to), subject, body)
    
    try:
        smtp_server = smtplib.SMTP_SSL('smtp.gmail.com', 465)
        smtp_server.ehlo()
        smtp_server.login(sender_email, sender_password)
        smtp_server.sendmail(sender_email, to, email_text)
        smtp_server.close()
        print ("Email sent successfully!")
    except Exception as ex:
        print ("Something went wrong….",ex)

# Reads the input csv file in chuncksizes
def process_data(input_file_path, db_path, table):

    chunksize = 100
    df = pd.read_csv(input_file_path, chunksize=chunksize)

    ## read and process df in chunks
    for chunk in df:
        ## enforce a schema
        ## all columns will be string types
        chunk['city'] = chunk.city.astype(str)
        chunk['state'] = chunk.state.astype(str)
        chunk['country'] = chunk.country.astype(str)
        chunk['postcode'] = chunk.postcode.astype(str)

        ## only take first 5 digits of postal code
        ## fill na for zipcode '00000' (invalid zipcode)
        chunk['postcode'].str.slice(0, 5)
        zero_zips = chunk['postcode'] == '00000'
        chunk['postcode'][zero_zips] = np.nan
        
        ## fill null values with empty strings
        chunk = chunk.fillna("")

        ## only take records for 'US' users
        chunk = chunk[chunk['country'] == "US"]

        update_db(db_path, table, chunk)

@click.command()
@click.argument('input_file', type=str, nargs=1)
@click.argument('db_location', type=str, nargs=1)
@click.argument('table', type=str, nargs=1)
@click.argument('config_file', type=str, nargs=1, default='config.yaml')
def main(input_file, db_location, table, config_file):

    my_path = os.path.abspath(os.path.dirname(__file__))
    input_file_path = os.path.join(my_path, "../data/", input_file)
    db_path = os.path.join(my_path, "../database/", db_location)

    print('')
    # print(f'The input file path is: {input_file_path}')
    # print(f'The db file location is: {db_path}')
    # print(f'The table name is: {table}')
    connect_to_db(db_path)
    print('')

    process_data(input_file_path, db_path, table)

    ## send emails to users about update
    ## only for process ran on saturdays
    if date.today().weekday() == 5:
        run_report(table, config_file)
    print('SQL insert process finished')


if __name__ == '__main__':
    main()