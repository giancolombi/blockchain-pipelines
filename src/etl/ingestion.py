import pandas as pd
import numpy as np
import sqlite3
from sqlite3 import Error
import smtplib
from datetime import date
import os.path

def connect_to_db(db_path):
    """
    Connect to an SQlite database, if db file does not exist it will be created
    :param db_file: absolute or relative path of db file
    :return: sqlite3 connection
    """
    sqlite3_conn = None

    try:
        sqlite3_conn = sqlite3.connect(db_path)
        return sqlite3_conn

    except Error as err:
        print(err)

        if sqlite3_conn is not None:
            sqlite3_conn.close()


def update_db(db_path, table_name, df):
    # # Create a SQL connection to our SQLite database
    con = sqlite3.connect(db_path)

    if con is not None:
        cur = con.cursor()
        # Create table if it is not exist
        cur.execute(
            'CREATE TABLE IF NOT EXISTS ' + table_name +
            '(city        VARCHAR,'
            'state        VARCHAR,'
            'country      VARCHAR,'
            'postcode     VARCHAR)'
        )

        ## check number of rows in db to trigger email service
        for row in cur.execute('SELECT count(1) FROM us_users;'):
            intial_row_count = row[0]
        
        df.to_sql(name=table_name, con=con, if_exists='append', index=False)

        ## check number of rows in db to trigger email service
        for row in cur.execute('SELECT count(1) FROM us_users;'):
            updated_row_count = row[0]

        ## check number of rows in db to trigger email service
        query = """
            SELECT distinct postcode, count(postcode) as zipcode_counts 
            FROM us_users 
            GROUP BY postcode
            ORDER BY zipcode_counts DESC Limit 1;
        """
        for row in cur.execute(query):
            top_zipcode = row
            
        top_zip_code = top_zipcode[0]
        count_of_entries = top_zipcode[1]

        statistics = []
        statistics.append(updated_row_count)
        statistics.append(top_zip_code)
        statistics.append(count_of_entries)

        con.close()
        return(statistics)

    else:
        print('Connection to database failed')


def trim_all_columns(df):
    """
    Trim whitespace from ends of each value across all series in dataframe
    """
    trim_strings = lambda x: x.strip() if isinstance(x, str) else x
    return df.applymap(trim_strings)


# Reads the input csv file in chuncksizes
def process_data(csv_input_file, db_path, table_name):

    csv_input_file = csv_input_file
    chunksize = 100
    df = pd.read_csv(csv_input_file, chunksize=chunksize)

    ## read and process df in chunks
    for chunk in df:
        ## enforce a schema
        ## all columns will be string types
        chunk['city'] = chunk.city.astype(str)
        chunk['state'] = chunk.state.astype(str)
        chunk['country'] = chunk.country.astype(str)
        chunk['postcode'] = chunk.postcode.astype(str)
        
        ## Trim all whitespaces
        chunk = trim_all_columns(chunk)

        ## standardize all city, state, country
        chunk['city'] = chunk['city'].str.upper()
        chunk['state'] = chunk['state'].str.upper()
        chunk['country'] = chunk['country'].str.upper()

        ## only take first 5 digits of postal code
        ## fill na for zipcode '00000' (invalid zipcode)
        chunk['postcode'].str.slice(0, 5)
        zero_zips = chunk['postcode'] == '00000'
        chunk['postcode'][zero_zips] = np.nan
        
        ## fill null values with empty strings
        chunk = chunk.fillna("")

        ## only take records for 'US' users
        chunk = chunk[chunk['country'] == "US"]

        statistics = update_db(db_path, table_name, chunk)

    ## send emails to users about update
    ## only for process ran on saturdays
    if date.today().weekday() == 5:
        run_report(table_name, statistics)
    print('SQL insert process finished')


## will send an email reporting given statistics about the db
def run_report(table_name, statistics):
    ## no hardcoded information here
    ## you would want to use a config file for this information
    gmail_user = 'blockchain.pipeline@gmail.com'
    gmail_password = 'Satoshi20Million'

    sent_from = gmail_user
    

    ## also with more time you would not want to expose your users
    ## this list will be in a config file
    to = ['blockchain_test@yopmail.com', 'blockchain_test1@yopmail.com', 'blockchain_test2@yopmail.com']
    subject = table_name + ' Has been updated!'
    body = 'Record count: ' + str(statistics[0]) + \
            ' With your top performing zipcode: ' + str(statistics[1]) + \
            ' with: ' + str(statistics[2]) + ' records in the db'
    
    email_text = """\
    From: %s
    To: %s
    Subject: %s

    %s
    """ % (sent_from, ", ".join(to), subject, body)
    
    try:
        smtp_server = smtplib.SMTP_SSL('smtp.gmail.com', 465)
        smtp_server.ehlo()
        smtp_server.login(gmail_user, gmail_password)
        smtp_server.sendmail(sent_from, to, email_text)
        smtp_server.close()
        print ("Email sent successfully!")
    except Exception as ex:
        print ("Something went wrong….",ex)


def main(csv_input_file, db_path, table_name):
    process_data(csv_input_file, db_path, table_name)

# @click.command()
# @click.argument('csv_input_file')
# @click.argument('db_path')
if __name__ == "__main__":

    my_path = os.path.abspath(os.path.dirname(__file__))

    csv_input_file = os.path.join(my_path, "../data/sample_us_states (1).csv")
    db_path = os.path.join(my_path, "../database/us_users.db")

    ## make this an input parameter, not a hardcoded path
    table_name = "us_users"

    ## first check db connection is working

    main(csv_input_file, db_path, table_name)

    