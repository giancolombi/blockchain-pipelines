#!/usr/bin/python3
import click
import os.path
import pandas as pd
import numpy as np
import sqlite3
from sqlite3 import Error
import smtplib
import datetime
import yaml
from pathlib import Path
import logging

def update_table_stats(db_path, table):
    # # Create a SQL connection to our SQLite database
    con = sqlite3.connect(db_path)

    if con is not None:
        cur = con.cursor()
        # Create table if it is not exist
        cur.execute(
            'CREATE TABLE IF NOT EXISTS table_stats' +
            '(table_name               VARCHAR,'
            'latest_row_count          INTEGER,'
            'lastest_etl_timestamp     INTEGER)'
        )

        for row in cur.execute('SELECT count(1) FROM ' + table + ';'):
            updated_row_count = row[0]

        d = {'table_name': [table], 'latest_row_count': [updated_row_count], 'lastest_etl_timestamp': [datetime.datetime.now()]}
        df = pd.DataFrame(data=d)
        
        df.to_sql(name="table_stats", con=con, if_exists='append', index=False)
        con.close()

    else:
        print('Connection to database failed')


@click.command()
@click.argument('input_file', type=str, nargs=1)
@click.argument('db_location', type=str, nargs=1)
@click.argument('table', type=str, nargs=1)
@click.argument('config_file', type=str, nargs=1, default='config.yaml')

def main(input_file, db_location, table, config_file):

    my_path = os.path.abspath(os.path.dirname(__file__))
    db_path = os.path.join(my_path, "../database/", db_location)
    update_table_stats(db_path, table)

if __name__ == '__main__':
    main()