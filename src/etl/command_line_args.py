#!/usr/bin/python3
import click
import os.path
import pandas as pd
import numpy as np
import sqlite3
from sqlite3 import Error
import smtplib
from datetime import date
import yaml
from pathlib import Path
import logging

def get_credentials(config_file):
    
    my_path = os.path.abspath(os.path.dirname(__file__))
    path = str(os.path.join(my_path, config_file))

    with open(path, 'r') as stream:
        try:
            cfg = yaml.safe_load(stream)
            sender_emails = cfg['email_service']['sender_email']
            recipeint_email = cfg['email_service']['recipeint_email']
            conn = {'sender_emails' : sender_emails, 'recipeint_email': recipeint_email}
        except yaml.YAMLError as exc:
            print(exc)
    return conn

def connect_to_db(db_path):
    """
    Connect to an SQlite database, if db file does not exist it will be created
    :param db_file: absolute or relative path of db file
    :return: sqlite3 connection
    """
    sqlite3_conn = None

    try:
        sqlite3_conn = sqlite3.connect(db_path)
        return sqlite3_conn

    except Error as err:
        print(err)

        if sqlite3_conn is not None:
            sqlite3_conn.close()

# Reads the input csv file in chuncksizes
def process_data(csv_input_file, db_path, table_name):

    csv_input_file = csv_input_file
    chunksize = 100
    df = pd.read_csv(csv_input_file, chunksize=chunksize)

    ## read and process df in chunks
    for chunk in df:
        ## enforce a schema
        ## all columns will be string types
        chunk['city'] = chunk.city.astype(str)
        chunk['state'] = chunk.state.astype(str)
        chunk['country'] = chunk.country.astype(str)
        chunk['postcode'] = chunk.postcode.astype(str)

@click.command()
@click.argument('input_file', type=str, nargs=1)
@click.argument('db_location', type=str, nargs=1)
@click.argument('table', type=str, nargs=1)
@click.argument('config_file', type=str, nargs=1, default='config.yaml')
def main(input_file, db_location, table, config_file):

    my_path = os.path.abspath(os.path.dirname(__file__))
    input_file_path = os.path.join(my_path, "../data/", input_file)
    db_path = os.path.join(my_path, "../database/", db_location)

    print('')
    print(f'The input file path is: {input_file_path}')
    print(f'The db file location is: {db_path}')
    print(f'The table name is: {table}')

    conn = get_credentials(config_file)
    print(f'Sender email is: {db_path}')
    print(f'Receipent email are: {table}')
    print('')

    # process_data(input_file_path, db_path, table)

    

if __name__ == '__main__':
    main()