#!/usr/bin/env python
import yaml
from pathlib import Path
import os.path

def get_credentials(config_file):
    
    my_path = os.path.abspath(os.path.dirname(__file__))
    path = str(os.path.join(my_path, config_file))
    print(path)

    with open(path, 'r') as stream:
        try:
            cfg = yaml.safe_load(stream)
            sender_emails = cfg['email_service']['sender_email']
            sender_password = cfg['email_service']['sender_password']
            recipeint_email = cfg['email_service']['recipeint_email']
            conn = {'sender_emails' : sender_emails, 'recipeint_email': recipeint_email}
        except yaml.YAMLError as exc:
            print(exc)
    return conn

config_file = "config.yaml"
conn = get_credentials(config_file)

print(conn['sender_emails'])
print(conn['recipeint_email'])